
class NavBar extends HTMLElement {
    constructor(){
        super();
    }

    connectedCallback() {
        this.innerHTML = `
            <header>
            <nav>
                <ul>
                    <li><a href="paceprediction.html">Pace Prediction</li>
                    <li><a href="about.html">About</li>
                    <li><a href="datasafety.html">Privacy & Protections</li>
                    <li><a href="contactus.html">Contact Us</li>
                </ul>
            </nav>
            </header>
        `;
    }
}
 customElements.define('nav-bar', NavBar);
